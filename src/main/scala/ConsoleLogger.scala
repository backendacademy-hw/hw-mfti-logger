import LogLevel.{Info, Trace}

class ConsoleLogger(
                     hasLevelTag: Boolean = true,
                     hasTimeTag: Boolean = false,
                     timeService: Clock = InstantClock,
                     component: Option[String] = None,
                     filterLevel: LogLevel = Trace
                   ) extends Logger(hasLevelTag, hasTimeTag, timeService, component, filterLevel) {
  override def innerLog(level: LogLevel, msg: => String): Unit =  {
    val record = mkRecord(level, msg)
    level match {
      case Info                                          => System.out.println(record)
      case logLevel if logLevel.priority > Info.priority => System.err.println(record)
      case _                                             => ()
    }
  }
}