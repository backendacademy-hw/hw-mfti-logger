class NullLogger extends Logger() {
  override def innerLog(level: LogLevel, msg: => String): Unit = ()
}
