import LogLevel.Trace

import java.io.PrintStream

class UserStreamLogger (stream: PrintStream,
                        hasLevelTag: Boolean = true,
                        hasTimeTag: Boolean = false,
                        timeService: Clock = InstantClock,
                        component: Option[String] = None,
                        filterLevel: LogLevel = Trace
                       ) extends Logger(hasLevelTag, hasTimeTag, timeService, component, filterLevel) {

  override def innerLog(level: LogLevel, msg: => String): Unit = {
    val record = mkRecord(level, msg)
    stream.println(record)
  }
}
