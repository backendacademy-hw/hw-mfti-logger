sealed abstract class LogLevel(val priority: Int)

object LogLevel {
  case object Trace extends LogLevel(10)
  case object Debug extends LogLevel(20)
  case object Info extends LogLevel(30)
  case object Warning extends LogLevel(40)
  case object Error extends LogLevel(50)
  case object Fatal extends LogLevel(60)
}
