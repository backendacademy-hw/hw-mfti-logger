import LogLevel.{Info, Trace}

import java.io.{File, FileWriter}

class FileLogger(path: String,
                 hasLevelTag: Boolean = true,
                 hasTimeTag: Boolean = false,
                 timeService: Clock = InstantClock,
                 component: Option[String] = None,
                 filterLevel: LogLevel = Trace
                ) extends Logger(hasLevelTag, hasTimeTag, timeService, component, filterLevel) {

  override def innerLog(level: LogLevel, msg: => String): Unit = {
    val record = mkRecord(level, msg)
    val writer = level match {
      case Info                                          => new FileWriter(new File(s"$path.log.info"), true)
      case logLevel if logLevel.priority > Info.priority => new FileWriter(new File(s"$path.log.error"), true)
      case _                                             => new FileWriter(new File(s"$path.log.debug"), true)
    }
    writer.write(record + "\n")
    writer.close()
  }
}
