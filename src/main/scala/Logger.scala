import LogLevel.{Debug, Error, Fatal, Info, Trace, Warning}

abstract class Logger(
                       hasLevelTag: Boolean = true,
                       hasTimeTag: Boolean = false,
                       timeService: Clock = InstantClock,
                       component: Option[String] = None,
                       filterLevel: LogLevel = Trace
                     ) {
  def log(level: LogLevel, msg: => String) = if (level.priority >= filterLevel.priority) innerLog(level, msg)

  def innerLog(level: LogLevel, msg: => String): Unit

  def mkRecord(level: LogLevel, msg: String): String = {
    val levelTag     = if (hasLevelTag) s"[${level.toString.toLowerCase}] " else ""
    val componentTag = component match {
      case Some(str) => s"[$str] "
      case None => ""
    }
    val timeTag      = if (hasTimeTag) s"[${timeService.now}] " else ""

    componentTag + timeTag + levelTag + msg
  }

  def trace(msg: String): Unit = log(Trace, msg)

  def debug(msg: String): Unit = log(Debug, msg)

  def info(msg: String): Unit = log(Info, msg)

  def warning(msg: String): Unit = log(Warning, msg)

  def error(msg: String): Unit = log(Error, msg)

  def fatal(msg: String): Unit = log(Fatal, msg)
}
