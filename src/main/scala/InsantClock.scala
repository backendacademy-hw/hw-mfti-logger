import java.time.Instant

object InstantClock extends Clock {
  def now: String = Instant.now().toString
}
